import { NativeModules, Platform } from 'react-native';
const GradientButtonModule = NativeModules.GradientButtonModule;
interface GradientButtonModuleInterface {
  getGradientColor(): string;
  getAngleNumber(): number;
}
export default GradientButtonModule as GradientButtonModuleInterface;
