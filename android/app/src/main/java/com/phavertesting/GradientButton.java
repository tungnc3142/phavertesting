package com.phavertesting;

import androidx.annotation.NonNull;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

import java.util.ArrayList;
import java.util.Arrays;

public class GradientButton extends ReactContextBaseJavaModule {
    ArrayList<String[]> gradients = new ArrayList<String[]>();
    int[] angles = {10,20,30,90,135, 70, 50,15,57,98,100};

    private void initData() {
        String[] array;
        array = new String[]{"#2deeb4",
                "#4eb6c2",
                "#6196ca",
                "#815fd8",
                "#9839e2",
                "#9839e2"};
        gradients.add(array);
        array = new String[]{"#4bb7f4",
                "#2deeb4",
                "#464E5F",
                "#1BB70E",
                "#DB3022",
                "#F1F1F2"};
        gradients.add(array);
        array = new String[]{"#4bb7f4",
                "#2deeb4"};
        gradients.add(array);
    }

    GradientButton(ReactApplicationContext context) {
        super(context);
        initData();
    }

    @NonNull
    @Override
    public String getName() {
        return "GradientButtonModule";
    }

    private int getRandomIndex(int size) {
        return (int)(Math.random() * size);
    }

    @ReactMethod(isBlockingSynchronousMethod = true)
    public String getGradientColor() {
        return Arrays.toString(gradients.get(getRandomIndex(gradients.size())));
    }

    @ReactMethod(isBlockingSynchronousMethod = true)
    public int getAngleNumber() {
        return angles[getRandomIndex(angles.length)];
    }

}
