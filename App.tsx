/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */

import React, { Dispatch, SetStateAction, useEffect, useState } from 'react';
import {
  Alert,
  Platform,
  StyleProp,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ViewStyle
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { Shadow } from 'react-native-shadow-2';
import GradientButtonModule from './GradientInterface';

interface GradientButtonProps {
  colors: string[];
  onPress: () => void;
  angle: number;
  styleGradient?: StyleProp<ViewStyle>;
  text: string;
}

const GradientButton: React.FC<GradientButtonProps> = (props) => {
  const { colors, angle, styleGradient, onPress, text } = props;
  if (!colors.length) return null;
  return (
    <LinearGradient
      colors={colors}
      useAngle
      angle={angle}
      start={{ x: 0.8, y: 1.0 }}
      end={{ x: 1.0, y: 1.0 }}
      style={[styles.gradient, styleGradient]}
    >
      <TouchableOpacity style={styles.buttonContainer} onPress={onPress}>
        <Text style={styles.buttonText}>{text}</Text>
      </TouchableOpacity>
    </LinearGradient>
  );
};

const convertStringToArray = (text: string | any) => {
  if (typeof text === 'string')
    return text
      .replace('[', '')
      .replace(']', '')
      .replace(' ', '')
      .split(',')
      .map((e: string) => e.trim());
  return text;
};

function App(): JSX.Element {
  const [colors1, setColor1] = useState<string[]>([]);
  const [colors2, setColor2] = useState<string[]>([]);
  const [angle1, setAngel1] = useState<number>(0);
  const [angle2, setAngel2] = useState<number>(0);

  const getGradientColor = async () => {
    if (Platform.OS === 'ios') {
      return convertStringToArray(
        await GradientButtonModule.getGradientColor()
      );
    }
    return convertStringToArray(GradientButtonModule.getGradientColor());
  };

  const getAngleNumber = async () => {
    if (Platform.OS === 'ios') {
      return await GradientButtonModule.getAngleNumber();
    }
    return GradientButtonModule.getAngleNumber();
  };

  const initData = async () => {
    const _color1 = await getGradientColor();
    const _color2 = await getGradientColor();
    setColor1(_color1);
    setColor2(_color2);
    const _angle1 = await getAngleNumber();
    const _angle2 = await getAngleNumber();
    setAngel1(_angle1);
    setAngel2(_angle2);
  };

  useEffect(() => {
    initData();
  }, []);

  const onClick =
    (
      text: string,
      callBackColor: Dispatch<SetStateAction<string[]>>,
      callBackAngle: Dispatch<SetStateAction<number>>
    ) =>
    async () => {
      Alert.alert(text);
      const color = await getGradientColor();
      const angle = await getAngleNumber();
      callBackColor(color);
      callBackAngle(angle);
    };
  return (
    <View style={styles.container}>
      <View style={styles.containerBtn}>
        <View>
          <GradientButton
            colors={colors1}
            angle={angle1}
            onPress={onClick('Button1', setColor1, setAngel1)}
            text="Button 1"
          />
        </View>

        <Shadow
          distance={15}
          startColor={'#2a3f3d'}
          style={styles.containerShadow}
          paintInside={true}
        >
          <GradientButton
            colors={colors2}
            angle={angle2}
            styleGradient={styles.shadow}
            text="Button 2"
            onPress={onClick('Button 2', setColor2, setAngel2)}
          />
        </Shadow>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#000',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  containerBtn: {
    flexDirection: 'row',
    gap: 50
  },
  txtBtn: {
    color: 'white'
  },
  gradient: {
    height: 44,
    width: 120,
    justifyContent: 'center',
    alignSelf: 'center',
    borderRadius: 10
  },
  buttonContainer: {
    flex: 1.0,
    alignSelf: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(0,0,0,0.8)',
    opacity: 0.85,
    width: '97%',
    margin: 2,
    borderRadius: 10
  },
  buttonContainer2: {
    flex: 1.0,
    alignSelf: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(0,0,0,0.8)',
    opacity: 0.7,
    width: '97%',
    margin: 2,
    borderRadius: 10,
    zIndex: 0.2
  },
  buttonText: {
    textAlign: 'center',
    color: 'white',
    alignSelf: 'center'
  },
  containerShadow: {
    backgroundColor: 'transparent',
    borderRadius: 18
  },
  shadow: {
    shadowColor: '#2deeb4',
    shadowOffset: {
      width: 0,
      height: 12
    },
    shadowOpacity: 0.58,
    shadowRadius: 16.0,

    elevation: 24
  }
});

export default App;
