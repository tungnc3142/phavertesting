//
//  RCTGradientModule.m
//  PhaverTesting
//
//  Created by Tung Nguyen on 16/03/2023.
//
#import <Foundation/Foundation.h>
#import "RCTGradientModule.h"

@implementation RCTGradientModule

RCT_EXPORT_MODULE(GradientButtonModule);

RCT_EXPORT_METHOD(getGradientColor: (RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject) {

  NSArray *array1 = [NSArray arrayWithObjects:@"#2deeb4",@"#4eb6c2",@"#6196ca", @"#815fd8", @"#9839e2", @"#9839e2",nil];
  
  NSArray *array2 = [NSArray arrayWithObjects:@"#4bb7f4",@"#2deeb4",@"#464E5F", @"#1BB70E", @"#DB3022", @"#F1F1F2",nil];
  
  NSArray *array3 = [NSArray arrayWithObjects:@"#4bb7f4",@"#2deeb4",nil];
  
  NSMutableArray *myArray;
      myArray= [NSMutableArray arrayWithObjects: array1, array2, array3, nil];
  
  
  uint32_t rnd = arc4random_uniform([myArray count]);
  resolve([myArray objectAtIndex:rnd]);
}


RCT_EXPORT_METHOD(getAngleNumber: (RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject) {

  NSArray *array1 = [NSArray arrayWithObjects:@10,@20,@30,@90,@135,@70, @50,@15,@57,@98,@100,nil];

  
  
  uint32_t rnd = arc4random_uniform([array1 count]);
  resolve([array1 objectAtIndex:rnd]);
}


@end
